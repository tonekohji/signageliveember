
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('valid-url', 'helper:valid-url', {
  integration: true
});

// Replace this with your real tests.
test('it renders', function(assert) {
  this.set('inputValue', '1234');

  this.render(hbs`{{valid-url inputValue}}`);

  assert.equal(this.$().text().trim(), '1234');
});

