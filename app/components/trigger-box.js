import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'li',
  classNames: ['trigger','trigger-selector'],
  click(){
    this.state.set('ifThis', this.data.trigger);
    this.state.set('thisTrigger', this.data.title);
  }
});
