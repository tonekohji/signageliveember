import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'li',
  classNames: ['action','action-selector'],
  click(){
    this.state.set('thenThat', this.data.trigger);
    this.state.set('thatAction', this.data.title);
  }
});
