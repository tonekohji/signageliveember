import Ember from 'ember';

export default Ember.Route.extend({
  model(){
    return {
      thisthat: this.modelFor('create'),
      triggers: [
        {
          title: "New feed item",
          description: "This Trigger fires every time a new item is added to the feed you specify.",
          trigger: "new feed item at",
          link: "create.triggerconfig"
        },
        {
          title: "New feed item matches",
          description: "This Trigger fires every time a new item in the feed you specify contains a particular keyword or simple phrase."
        }
      ]
    }
  }
});
