import Ember from 'ember';

export default Ember.Route.extend({
  model(){
    return {
      thisthat: this.modelFor('create'),
      triggers: [
        {
          title: "Send me an email",
          description: "This Action will send you an HTML based email. Images and links are supported.",
          trigger: "send an email to",
          link: "create.actionconfig"
        }
      ]
    }
  }
});
