import DS from 'ember-data';

export default DS.Model.extend({
  ifThis: DS.attr(),
  thisTrigger: DS.attr(),
  thisPayload: DS.attr(),
  thenThat: DS.attr(),
  thatAction: DS.attr(),
  thatPayload: DS.attr()
});
