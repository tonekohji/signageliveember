import Ember from 'ember';

const pattern = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z‌​]{2,6}\b([-a-zA-Z0-9‌​@:%_\+.~#?&=]*)/;

export function validUrl(url) {
  return pattern.test(url);
}

export default Ember.Helper.helper(validUrl);
