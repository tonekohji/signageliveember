import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('create', function() {
    this.route('services');
    this.route('triggers');
    this.route('triggerconfig');
    this.route('that');
    this.route('aservices');
    this.route('actions');
    this.route('actionconfig');
    this.route('summary');
  });
});

export default Router;
